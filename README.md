
# Are you tired of constantly tracking the latest shit you've compiled into ```$HOME```?

Hi. My name's Emil and I like to live on the edge, running half-baked versions of software build by people far more competent than me.

Do you know what PATH variable does? It "tells" your shell where the interesting scripts and binaries live. On my system, many of them live around my home folder, since I hand compile whatever tickles my fancy. 

Normally I just put another folder's path into my .whatever_shell_you're_usingrc but it's a pain in the ass if you run multiple computers, different configurations, shells.

Real life example of the magic this enterprise-ready (not yet on the Blockchain IoT, does not compile to JavaScript) script provides:

```
❯ # can i haz nim?
❯ nim
zsh: command not found: nim
❯ source pathmebro example-paths
❯ nim
Nim Compiler Version 0.16.0 (2017-01-29) [Linux: amd64]
Copyright (c) 2006-2017 by Andreas Rumpf
```

Just stick your folder list in any file, throw it in your dotfiles, source ```pathmebro``` in your shell's rc script, you're done. Or, have an alternative version of PATH for development purposes.

Be like He-Man. Have the power.

And a tiger.
